# AuD 2021 Exam Prep

Tests und Lösungen zu den Problemstellungen aus
[Inspirationen zur Klausurvorbereitung Aufgabe 2+3](https://moodle.informatik.tu-darmstadt.de/mod/resource/view.php?id=38877)

## Wie übe ich hiermit?

Im Branch `train` sind keine Lösungen enthalten.
```console
$ git clone -b train <this-repo>
```
Dann einfach alle TODOs abarbeiten.

## Sequenzen

Für jede Datenstruktur, die Sequenzen darstellen,
sollte das Interface [`SequenceProcessor`](src/main/java/aud/exam/prep/SequenceProcessor.java)
einmal iterativ und einmal rekursiv implementiert werden.

Jede fast Methode in `SequenceProcessor` stellt eine Aufgabe dar,
wie sie im Inspirations-Blatt erklärt wird.

Die nicht als Aufgabe gedachten Methoden in `SequenceProcessor`
sind für die Tests wichtig.

- `SequenceProcessor.check(s)`: Prüft, dass die Sequenz `s` konform entsprechend
der Datenstruktur ist.

- `SequenceProcessor.create(Iterable)`: Erstellt eine Instanz einer
Sequenz von einem Iterator.

- `SequenceProcessor.iterate(s)`: Erlaubt das Iterieren
durch eine Sequenz `s`.

## Tipps

1. Zwei Objekte auf Gleichheit prüfen:
<https://docs.oracle.com/javase/8/docs/api/java/util/Objects.html#equals-java.lang.Object-java.lang.Object->

1. Neues Array vom Typ `T` erstellen:
[`Arrays.newArray(size)`](src/main/java/aud/exam/prep/array/Arrays.java)
