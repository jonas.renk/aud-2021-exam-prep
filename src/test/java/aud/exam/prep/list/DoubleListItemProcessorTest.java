package aud.exam.prep.list;

import aud.exam.prep.SequenceProcessor;
import aud.exam.prep.SequenceProcessorTest;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

abstract class DoubleListItemProcessorTest extends SequenceProcessorTest<DoubleListItem<Integer>> {

    protected DoubleListItemProcessorTest(SequenceProcessor<Integer, DoubleListItem<Integer>> processor) {
        super(processor);
    }

    @Test
    void testThat_checkOfNullIsTrue() {
        assertTrue(processor.check(null));
    }

    @Test
    void testThat_checkOfNoKeyIsFalse() {
        var item = new DoubleListItem<Integer>();
        assertFalse(processor.check(item));
    }

    @Test
    void testThat_checkOfNoPrevOfNextIsFalse() {
        var item = new DoubleListItem<Integer>();
        item.key = 1;

        item.next = new DoubleListItem<>();
        item.next.key = 1;

        assertFalse(processor.check(item));
    }

    @Test
    void testThat_checkOfCircleIsFalse() {
        var item = new DoubleListItem<Integer>();
        item.next = item;
        assertFalse(processor.check(item));
    }

    @Test
    void testThat_checkPrevNextNotMatchingIsFalse() {
        var item = new DoubleListItem<Integer>();
        item.next = new DoubleListItem<>();
        item.next.prev = new DoubleListItem<>();
        assertFalse(processor.check(item));
    }
}