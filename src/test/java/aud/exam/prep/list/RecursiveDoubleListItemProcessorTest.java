package aud.exam.prep.list;

public class RecursiveDoubleListItemProcessorTest extends DoubleListItemProcessorTest {

    protected RecursiveDoubleListItemProcessorTest() {
        super(new RecursiveDoubleListItemProcessor<>());
    }
}
