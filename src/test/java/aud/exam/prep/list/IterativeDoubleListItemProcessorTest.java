package aud.exam.prep.list;

import aud.exam.prep.SequenceProcessorTest;

class IterativeDoubleListItemProcessorTest extends SequenceProcessorTest<DoubleListItem<Integer>> {

    protected IterativeDoubleListItemProcessorTest() {
        super(new IterativeDoubleListItemProcessor<>());
    }
}