package aud.exam.prep.list;

import aud.exam.prep.SequenceProcessorTest;

class RecursiveSingleListItemProcessorTest extends SequenceProcessorTest<SingleListItem<Integer>> {

    protected RecursiveSingleListItemProcessorTest() {
        super(new RecursiveSingleListItemProcessor<>());
    }
}