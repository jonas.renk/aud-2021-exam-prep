package aud.exam.prep.list;

import aud.exam.prep.SequenceProcessorTest;

class IterativeSingleListItemProcessorTest extends SequenceProcessorTest<SingleListItem<Integer>> {

    protected IterativeSingleListItemProcessorTest() {
        super(new IterativeSingleListItemProcessor<>());
    }
}