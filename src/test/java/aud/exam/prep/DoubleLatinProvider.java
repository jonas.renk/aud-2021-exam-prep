package aud.exam.prep;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DoubleLatinProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        return Stream
            .generate(() ->
                Arguments.of(randomLatin(), randomLatin()))
            .limit(Tests.STEAM_SIZE);
    }

    private String randomLatin() {
        var size = Tests.RANDOM.nextInt(100);
        return Tests.RANDOM
            .ints(size, 'A', 'Z'+1)
            .mapToObj(n ->
                String.valueOf((char) n))
            .collect(Collectors.joining());
    }
}
