package aud.exam.prep.array;

import aud.exam.prep.SequenceProcessorTest;

public class IterativeFullyUsedArrayProcessorTest extends SequenceProcessorTest<FullyUsedArray<Integer>> {

    protected IterativeFullyUsedArrayProcessorTest() {
        super(new IterativeFullyUsedArrayProcessor<>());
    }
}