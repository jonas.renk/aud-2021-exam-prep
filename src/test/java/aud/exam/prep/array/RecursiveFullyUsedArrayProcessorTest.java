package aud.exam.prep.array;

import aud.exam.prep.SequenceProcessorTest;

class RecursiveFullyUsedArrayProcessorTest extends SequenceProcessorTest<FullyUsedArray<Integer>> {

    protected RecursiveFullyUsedArrayProcessorTest() {
        super(new RecursiveFullyUsedArrayProcessor<>());
    }
}