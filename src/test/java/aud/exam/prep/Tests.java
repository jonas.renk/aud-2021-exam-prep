package aud.exam.prep;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class Tests {

    public static final int STEAM_SIZE = 50;

    public static final Random RANDOM = new Random();
    public static final Comparator<Integer> CMP = Integer::compareTo;

    static List<Integer> randomList() {
        var size = RANDOM.nextInt(50);
        return RANDOM
            .ints(size, 0, 100)
            .boxed()
            .collect(Collectors.toList());
    }

    public static Integer getMax(List<Integer> list) {
        return list
            .stream()
            .max(CMP)
            .orElseThrow();
    }

    public static Integer getSecondMax(List<Integer> list) {
        return list
            .stream()
            .sorted(CMP.reversed())
            .skip(1)
            .findFirst()
            .orElseThrow();
    }
}