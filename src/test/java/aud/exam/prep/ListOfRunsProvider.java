package aud.exam.prep;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class ListOfRunsProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        return Stream
            .generate(this::listOfRuns)
            .map(Arguments::of)
            .limit(Tests.STEAM_SIZE);
    }

    private List<List<Integer>> listOfRuns() {
        var runs = Tests.RANDOM.nextInt(20);
        return Stream
            .generate(this::run)
            .limit(runs)
            .collect(Collectors.toList());
    }

    private List<Integer> run() {
        var size = Tests.RANDOM.nextInt(50);
        var run = IntStream
            .range(0, size)
            .boxed()
            .flatMap(i -> {
                var n = Tests.RANDOM.nextInt(5);
                return Collections.nCopies(n, i).stream();
            })
            .collect(Collectors.toList());

        run.add(0, -1);
        run.add(999);
        return run;
    }
}
