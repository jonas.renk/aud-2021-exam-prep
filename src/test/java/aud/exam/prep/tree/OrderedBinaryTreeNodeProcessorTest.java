package aud.exam.prep.tree;

import org.junit.jupiter.api.Test;

import static aud.exam.prep.Tests.CMP;
import static org.junit.jupiter.api.Assertions.*;

public abstract class OrderedBinaryTreeNodeProcessorTest extends OrderedTreeProcessorTest<BinaryTreeNode<Integer>> {

    protected OrderedBinaryTreeNodeProcessorTest(OrderedTreeProcessor<Integer, BinaryTreeNode<Integer>> processor) {
        super(processor);
    }

    @Test
    void testThat_checkOfNullIsTrue() {
        assertTrue(processor.check(null, CMP));
    }

    @Test
    void testThat_checkOfNoChildrenIsTrue() {
        var t = new BinaryTreeNode<Integer>();
        t.key = 2;
        assertTrue(processor.check(t, CMP));
    }

    @Test
    void testThat_checkOfNoKeyIsFalse() {
        var t = new BinaryTreeNode<Integer>();
        assertFalse(processor.check(t, CMP));
    }

    @Test
    void testThat_checkOfLeftGreaterIsFalse() {
        var t = new BinaryTreeNode<Integer>();
        t.key = 5;

        t.left = new BinaryTreeNode<>();
        t.left.key = 10;

        assertFalse(processor.check(t, CMP));
    }

    @Test
    void testThat_checkOfLeftSmallerIsTrue() {
        var t = new BinaryTreeNode<Integer>();
        t.key = 5;

        t.left = new BinaryTreeNode<>();
        t.left.key = 2;

        assertTrue(processor.check(t, CMP));
    }

    @Test
    void testThat_checkOfRightGreaterIsTrue() {
        var t = new BinaryTreeNode<Integer>();
        t.key = 5;

        t.right = new BinaryTreeNode<>();
        t.right.key = 10;

        assertTrue(processor.check(t, CMP));
    }

    @Test
    void testThat_checkOfRightSmallerIsFalse() {
        var t = new BinaryTreeNode<Integer>();
        t.key = 5;

        t.right = new BinaryTreeNode<>();
        t.right.key = 2;

        assertFalse(processor.check(t, CMP));
    }

    @Test
    void testThat_checkOfBadRightIsFalse() {
        var t = new BinaryTreeNode<Integer>();
        t.key = 5;

        t.right = new BinaryTreeNode<>();

        assertFalse(processor.check(t, CMP));
    }


    @Test
    void testThat_checkOfLeftIsFalse() {
        var t = new BinaryTreeNode<Integer>();
        t.key = 5;

        t.left = new BinaryTreeNode<>();

        assertFalse(processor.check(t, CMP));
    }
}