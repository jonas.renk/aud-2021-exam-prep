package aud.exam.prep.tree;

public class RecursiveOrderedBinaryTreeNodeProcessorTest extends OrderedBinaryTreeNodeProcessorTest {

    public RecursiveOrderedBinaryTreeNodeProcessorTest() {
        super(new RecursiveOrderedBinaryTreeNodeProcessor<>());
    }
}