package aud.exam.prep.tree;

import aud.exam.prep.ListProvider;
import aud.exam.prep.Pointer;
import aud.exam.prep.Tests;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static aud.exam.prep.Tests.CMP;
import static org.junit.jupiter.api.Assertions.*;

public abstract class OrderedTreeProcessorTest<T> {

    protected final OrderedTreeProcessor<Integer, T> processor;

    protected OrderedTreeProcessorTest(OrderedTreeProcessor<Integer, T> processor) {
        this.processor = processor;
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_findOfNonExistsIsFalse(List<Integer> list) {
        T t = asTree(list);
        assertFalse(processor.find(t, -1, CMP));
        assertFalse(processor.find(t, 999, CMP));
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_findOfExistsIsTrue(List<Integer> list) {
        T t = asTree(list);
        for (var n : list) {
            assertTrue(processor.find(t, n, CMP));
        }
    }

    @Test
    void testThat_newEmptyTreeWorks() {
        T t = processor.newEmptyTree();

        assertTrue(processor.check(t, CMP));

        assertEquals(-1, processor.height(t));

        assertTrue(processor.isBalanced(t));

        assertEquals(0, processor.numberOfNodes(t));

        for (int i = 0; i < 10; i++) {
            assertEquals(0, processor.numberOfNodesOnLevel(t, i));
        }

        assertIterableEquals(List.of(), processor.iterate(t));
    }


    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_insertAndIterateWork(List<Integer> list) {
        T t = asTree(list);
        list.sort(CMP);

        assertTrue(processor.check(t, CMP));
        assertIterableEquals(list, processor.iterate(t));
    }

    protected T asTree(List<Integer> list) {
        T t = processor.newEmptyTree();

        for (var n : list) {
            t = processor.insert(t, n, CMP);
        }

        return t;
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_maxWorks(List<Integer> list) {
        list.add(-1);
        T t = asTree(list);

        Integer max = Tests.getMax(list);
        assertEquals(max, processor.max(t));
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_secondMaxWorks(List<Integer> list) {
        list.add(-1);
        list.add(-2);
        T t = asTree(list);

        Integer max = Tests.getSecondMax(list);
        assertEquals(max, processor.secondMax(t));
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_removeOfNonExistsIsFalseAndDoesNotModify(List<Integer> list) {
        T t = asTree(list);
        list.sort(CMP);

        var p = new Pointer<>(t);
        assertFalse(processor.remove(p, -1, CMP));
        assertFalse(processor.remove(p, 999, CMP));
        t = p.deref;

        assertTrue(processor.check(t, CMP));
        assertIterableEquals(list, processor.iterate(t));
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_removeOfExistsIsTrueAndDoesModify(List<Integer> list) {
        T t = asTree(list);
        list.sort(CMP);

        while (!list.isEmpty()) {
            var toRemove = list.get(list.size()/2);
            list.remove(toRemove);

            var p = new Pointer<>(t);
            assertTrue(processor.remove(p, toRemove, CMP));
            t = p.deref;

            assertTrue(processor.check(t, CMP));
            assertIterableEquals(list, processor.iterate(t));
        }
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_removeAllOfNonExistsIsFalseAndDoesNotModify(List<Integer> list) {
        T t = asTree(list);
        list.sort(CMP);

        var p = new Pointer<>(t);
        assertFalse(processor.removeAll(p, -1, CMP));
        assertFalse(processor.removeAll(p, 999, CMP));
        t = p.deref;

        assertTrue(processor.check(t, CMP));
        assertIterableEquals(list, processor.iterate(t));
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_removeAllOfExistsIsTrueAndDoesModify(List<Integer> list) {
        T t = asTree(list);
        list.sort(CMP);

        while (!list.isEmpty()) {
            var toRemove = list.get(list.size()/2);
            list.removeAll(Collections.singleton(toRemove));

            var p = new Pointer<>(t);
            assertTrue(processor.removeAll(p, toRemove, CMP));
            t = p.deref;

            assertTrue(processor.check(t, CMP));
            assertIterableEquals(list, processor.iterate(t));
        }
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_removeIfWorks(List<Integer> list) {
        T t = asTree(list);
        list.sort(CMP);

        for (var toRemove : getRemovals(list)) {
            Predicate<Integer> pred = n ->
                    Objects.equals(n, toRemove);

            list.removeIf(pred);

            t = processor.removeIf(t, pred);

            assertTrue(processor.check(t, CMP));
            assertIterableEquals(list, processor.iterate(t));
        }
    }

    List<Integer> getRemovals(List<Integer> list) {
        var start = -100 + list
                .stream()
                .min(CMP)
                .orElse(0);

        var end = 100 + list
                .stream()
                .max(CMP)
                .orElse(0);

        var removals = IntStream
                .rangeClosed(start, end)
                .boxed()
                .collect(Collectors.toList());

        Collections.shuffle(removals, Tests.RANDOM);

        return removals;
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void printATree(List<Integer> list) {
        T t = asTree(list);
        processor.print(t);
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_overrideOfNotExistsIsFalseAndDoesNotModify(List<Integer> list) {
        T t = asTree(list);
        list.sort(CMP);

        var p = new Pointer<>(t);
        assertFalse(processor.override(p, -1, -5, CMP));
        assertFalse(processor.override(p, 999, -5, CMP));
        t = p.deref;

        assertTrue(processor.check(t, CMP));
        assertIterableEquals(list, processor.iterate(t));
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_overrideOfExistsIsTrueAndDoesModify(List<Integer> list) {
        T t = asTree(list);
        for (int i = 0; i < list.size(); i++) {
            var to = -(i*i) - 5;

            var p = new Pointer<>(t);
            assertTrue(processor.override(p, list.get(i), to, CMP));
            list.set(i, to);
            t = p.deref;

            assertTrue(processor.check(t, CMP));
            list.sort(CMP);
            assertIterableEquals(list, processor.iterate(t));
        }
    }


    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_overrideAllOfNotExistsIsFalseAndDoesNotModify(List<Integer> list) {
        T t = asTree(list);
        list.sort(CMP);

        var p = new Pointer<>(t);
        assertFalse(processor.overrideAll(p, -1, -5, CMP));
        assertFalse(processor.overrideAll(p, 999, -5, CMP));
        t = p.deref;

        assertTrue(processor.check(t, CMP));
        assertIterableEquals(list, processor.iterate(t));
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_overrideAllOfExistsIsTrueAndDoesModify(List<Integer> list) {
        T t = asTree(list);
        for (int i = 0; i < list.size(); i++) {
            var from = list.get(i);
            var to = -(i*i) - 5;

            var p = new Pointer<>(t);
            assertTrue(processor.overrideAll(p, from, to, CMP));
            t = p.deref;
            list.replaceAll(k ->
                    Objects.equals(k, from) ? to : k);

            assertTrue(processor.check(t, CMP));
            list.sort(CMP);
            assertIterableEquals(list, processor.iterate(t));
        }
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_invertWorks(List<Integer> list) {
        T t = asTree(list);
        processor.invert(t);

        var cmp = CMP.reversed();

        assertTrue(processor.check(t, cmp));
        list.sort(cmp);
        assertIterableEquals(list, processor.iterate(t));
    }

    @ParameterizedTest
    @ArgumentsSource(ListProvider.class)
    void testThat_cloneWorks(List<Integer> list) {
        T t = asTree(list);
        t = processor.clone(t);

        assertTrue(processor.check(t, CMP));
        list.sort(CMP);
        assertIterableEquals(list, processor.iterate(t));
    }
}