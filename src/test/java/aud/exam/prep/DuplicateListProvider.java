package aud.exam.prep;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DuplicateListProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        return Stream
            .generate(this::randomListWithDuplicates)
            .map(Arguments::of)
            .limit(Tests.STEAM_SIZE);
    }

    private List<Integer> randomListWithDuplicates() {
        var size = Tests.RANDOM.nextInt(50);
        return Tests.RANDOM
            .ints(size, 0, 100)
            .boxed()
            .flatMap(n -> {
                var count = Tests.RANDOM.nextInt(10);
                return Collections
                    .nCopies(count, n)
                    .stream();
            })
            .collect(Collectors.toList());
    }
}
