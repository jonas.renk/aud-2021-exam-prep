package aud.exam.prep;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.stream.Stream;

public class DoubleListProvider implements ArgumentsProvider {

    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) {
        return Stream
            .generate(() ->
                Arguments.of(Tests.randomList(), Tests.randomList()))
            .limit(Tests.STEAM_SIZE);
    }

}
