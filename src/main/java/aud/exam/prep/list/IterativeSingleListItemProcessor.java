package aud.exam.prep.list;

import aud.exam.prep.Pair;

import java.util.Comparator;

public class IterativeSingleListItemProcessor<T> extends SingleListItemProcessor<T> {

    @Override
    public boolean find(SingleListItem<T> list, T t) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean findBinary(SingleListItem<T> list, T t, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean override(SingleListItem<T> list, T from, T to) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean overrideAll(SingleListItem<T> list, T from, T to) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean overrideAt(SingleListItem<T> list, T to, int index) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void insertInOrder(SingleListItem<T> list, T t, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean remove(SingleListItem<T> list, T t) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean removeAll(SingleListItem<T> list, T t) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isAscending(SingleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public T max(SingleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public T secondMax(SingleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isItemWiseLessOrEqual(SingleListItem<T> a, SingleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isItemWiseLess(SingleListItem<T> a, SingleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isLexSmaller(SingleListItem<T> a, SingleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void exchangePairs(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void rotateTriples(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void removeEverySecond(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void doubleAllKeys(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public SingleListItem<T> rotateRight(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public SingleListItem<T> rotateLeft(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void removeDuplicates(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public SingleListItem<T> invert(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public SingleListItem<T> clone(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public SingleListItem<T> alternate(SingleListItem<T> a, SingleListItem<T> b) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public SingleListItem<T> merge(SingleListItem<T> a, SingleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Pair<SingleListItem<T>, SingleListItem<T>> divideAlternating(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Pair<SingleListItem<T>, SingleListItem<T>> divideAlternatingByRuns(SingleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Pair<SingleListItem<T>, SingleListItem<T>> divideByPivot(SingleListItem<T> list, T pivot, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean check(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public SingleListItem<T> create(Iterable<T> iterable) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Iterable<T> iterate(SingleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }
}