package aud.exam.prep.list;

import aud.exam.prep.SequenceProcessor;

abstract class DoubleListItemProcessor<T> implements SequenceProcessor<T, DoubleListItem<T>> {

    @Override
    public Iterable<T> iterate(DoubleListItem<T> list) {
        return () ->
            new ListItemIterator<>(list);
    }
}