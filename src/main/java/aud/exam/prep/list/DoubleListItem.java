package aud.exam.prep.list;

public class DoubleListItem<T> implements ListItem<T> {
    public T key;
    public DoubleListItem<T> prev;
    public DoubleListItem<T> next;

    @Override
    public T head() {
        return key;
    }

    @Override
    public ListItem<T> tail() {
        return next;
    }
}