package aud.exam.prep.list;

public interface ListItem<T> {

    T head();

    ListItem<T> tail();
}
