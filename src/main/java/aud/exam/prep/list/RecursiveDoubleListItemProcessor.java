package aud.exam.prep.list;

import aud.exam.prep.Pair;

import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;

public class RecursiveDoubleListItemProcessor<T> extends DoubleListItemProcessor<T> {

    @Override
    public boolean find(DoubleListItem<T> list, T t) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean findBinary(DoubleListItem<T> list, T t, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean override(DoubleListItem<T> list, T from, T to) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean overrideAll(DoubleListItem<T> list, T from, T to) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean overrideAt(DoubleListItem<T> list, T to, int index) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void insertInOrder(DoubleListItem<T> list, T t, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean remove(DoubleListItem<T> list, T t) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean removeAll(DoubleListItem<T> list, T t) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isAscending(DoubleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public T max(DoubleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public T secondMax(DoubleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isItemWiseLessOrEqual(DoubleListItem<T> a, DoubleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isItemWiseLess(DoubleListItem<T> a, DoubleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isLexSmaller(DoubleListItem<T> a, DoubleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void exchangePairs(DoubleListItem<T> list) {
        if (list == null || list.next == null || list.next.next == null) {
            return;
        }

        var next = list.next;
        var nextNext = next.next;
        var rest = nextNext.next;

        list.next = nextNext;
        nextNext.prev = list;

        nextNext.next = next;
        next.prev = nextNext;

        next.next = rest;
        if (rest != null) {
            rest.prev = next;
        }

        exchangePairs(list.next.next);
    }

    @Override
    public void rotateTriples(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void removeEverySecond(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void doubleAllKeys(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> rotateRight(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> rotateLeft(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void removeDuplicates(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> invert(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> clone(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> alternate(DoubleListItem<T> a, DoubleListItem<T> b) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> merge(DoubleListItem<T> a, DoubleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Pair<DoubleListItem<T>, DoubleListItem<T>> divideAlternating(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Pair<DoubleListItem<T>, DoubleListItem<T>> divideAlternatingByRuns(DoubleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Pair<DoubleListItem<T>, DoubleListItem<T>> divideByPivot(DoubleListItem<T> list, T pivot, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean check(DoubleListItem<T> list) {
        return checkRec(list, new HashSet<>(), null);
    }

    private boolean checkRec(DoubleListItem<T> list, HashSet<DoubleListItem<T>> seen, DoubleListItem<T> prev) {
        if (list == null) {
            if (seen.isEmpty()) {
                return true;
            }

            if (prev == null) {
                return true;
            }

            return prev.next == null;
        }

        if (list.prev != prev) {
            return false;
        }

        if (prev != null && prev.next != list) {
            return false;
        }

        if (list.key == null) {
            return false;
        }

        if (seen.contains(list)) {
            return false;
        }

        seen.add(list);
        return checkRec(list.next, seen, list);
    }

    @Override
    public DoubleListItem<T> create(Iterable<T> iterable) {
        var iter = iterable.iterator();
        return createRec(iter, null);
    }

    private DoubleListItem<T> createRec(Iterator<T> iter, DoubleListItem<T> prev) {
        if (!iter.hasNext()) {
            return null;
        }

        var item = new DoubleListItem<T>();
        item.key = iter.next();
        item.next = createRec(iter, item);
        item.prev = prev;

        return item;
    }
}