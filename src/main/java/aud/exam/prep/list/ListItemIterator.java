package aud.exam.prep.list;

import java.util.Iterator;

public class ListItemIterator<T> implements Iterator<T> {

    private ListItem<T> current;

    public ListItemIterator(ListItem<T> current) {
        this.current = current;
    }

    @Override
    public boolean hasNext() {
        return current != null;
    }

    @Override
    public T next() {
        T t = current.head();
        current = current.tail();
        return t;
    }
}
