package aud.exam.prep.list;

import aud.exam.prep.Pair;

import java.util.Comparator;

public class IterativeDoubleListItemProcessor<T> extends DoubleListItemProcessor<T> {

    @Override
    public boolean find(DoubleListItem<T> list, T t) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean findBinary(DoubleListItem<T> list, T t, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean override(DoubleListItem<T> list, T from, T to) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean overrideAll(DoubleListItem<T> list, T from, T to) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean overrideAt(DoubleListItem<T> list, T to, int index) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void insertInOrder(DoubleListItem<T> list, T t, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean remove(DoubleListItem<T> list, T t) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean removeAll(DoubleListItem<T> list, T t) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isAscending(DoubleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public T max(DoubleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public T secondMax(DoubleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isItemWiseLessOrEqual(DoubleListItem<T> a, DoubleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isItemWiseLess(DoubleListItem<T> a, DoubleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean isLexSmaller(DoubleListItem<T> a, DoubleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void exchangePairs(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void rotateTriples(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void removeEverySecond(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void doubleAllKeys(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> rotateRight(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> rotateLeft(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public void removeDuplicates(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> invert(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> clone(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> alternate(DoubleListItem<T> a, DoubleListItem<T> b) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> merge(DoubleListItem<T> a, DoubleListItem<T> b, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Pair<DoubleListItem<T>, DoubleListItem<T>> divideAlternating(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Pair<DoubleListItem<T>, DoubleListItem<T>> divideAlternatingByRuns(DoubleListItem<T> list, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Pair<DoubleListItem<T>, DoubleListItem<T>> divideByPivot(DoubleListItem<T> list, T pivot, Comparator<T> cmp) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public boolean check(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public DoubleListItem<T> create(Iterable<T> iterable) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }

    @Override
    public Iterable<T> iterate(DoubleListItem<T> list) {
        // TODO: Your training
        throw new RuntimeException("unimplemented");
    }
}