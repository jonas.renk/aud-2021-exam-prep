package aud.exam.prep.list;

public class SingleListItem<T> implements ListItem<T> {

    public T key;
    public SingleListItem<T> next;

    @Override
    public T head() {
        return key;
    }

    @Override
    public ListItem<T> tail() {
        return next;
    }
}