package aud.exam.prep;

public class Pointer<T> {

    public T deref;

    public Pointer(T initial) {
        deref = initial;
    }
}
