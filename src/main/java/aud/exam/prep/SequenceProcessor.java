package aud.exam.prep;

import java.util.Comparator;

/**
 * Instances of this interface can work with any "valid" sequences of type <code>S</code>.
 * For what is valid, see {@link #check(S)}.
 * This is a literal translation of the "inspiration sheet" into java.
 *
 * @param <T> Type of elements stored in <code>S</code>
 * @param <S> Type of the sequence
 */
public interface SequenceProcessor<T, S> {

    boolean find(S s, T t);

    boolean findBinary(S s, T t, Comparator<T> cmp);

    boolean override(S s, T from, T to);

    boolean overrideAll(S s, T from, T to);

    boolean overrideAt(S s, T to, int index);

    void insertInOrder(S s, T t, Comparator<T> cmp);

    boolean remove(S s, T t);

    boolean removeAll(S s, T t);

    boolean isAscending(S s, Comparator<T> cmp);

    T max(S s, Comparator<T> cmp);

    T secondMax(S s, Comparator<T> cmp);

    boolean isItemWiseLessOrEqual(S a, S b, Comparator<T> cmp);

    boolean isItemWiseLess(S a, S b, Comparator<T> cmp);

    boolean isLexSmaller(S a, S b, Comparator<T> cmp);

    void exchangePairs(S s);

    void rotateTriples(S s);

    void removeEverySecond(S s);

    void doubleAllKeys(S s);

    S rotateRight(S s);

    S rotateLeft(S s);

    void removeDuplicates(S s);

    S invert(S s);

    S clone(S s);

    S alternate(S a, S b);

    S merge(S a, S b, Comparator<T> cmp);

    Pair<S, S> divideAlternating(S s);

    Pair<S, S> divideAlternatingByRuns(S s, Comparator<T> cmp);

    Pair<S, S> divideByPivot(S s, T pivot, Comparator<T> cmp);

    /**
     * Check whether ot not the given sequence is "valid" according to
     * what is considered valid depends on the specific data structure.
     * This is also used by the tests to verify implementations.
     * This has to be tested separate for each data structure.
     *
     * @param s A sequence
     * @return true iff the given sequence is valid
     */
    boolean check(S s);

    /**
     * Create a sequence from an {@link Iterable}.
     * This is required by the tests.
     *
     * @param iterable An {@link Iterable}
     * @return A sequence with the elements of the given iterable
     */
    S create(Iterable<T> iterable);

    /**
     * Return an {@link Iterable}, or a lambda returning {@link java.util.Iterator},
     * over the elements of the sequence <code>s</code>.
     * This is required by the tests.
     *
     * @param s A sequence
     * @return An {@link Iterable} over s
     */
    Iterable<T> iterate(S s);
}
