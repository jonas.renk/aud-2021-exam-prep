package aud.exam.prep.tree;

import java.util.Iterator;
import java.util.NoSuchElementException;

class BinaryTreeIterator<T> implements Iterator<T> {

    private final Stack<StackFrame> stack = new Stack<>();

    BinaryTreeIterator(BinaryTreeNode<T> node) {
        if (node != null) {
            var frame = new StackFrame();
            frame.node = node;
            stack.push(frame);
        }
    }

    @Override
    public boolean hasNext() {
        return !stack.empty();
    }

    @Override
    public T next() {
        if (!hasNext()) {
            throw new NoSuchElementException();
        }

        var frame = stack.peek();

        while (!frame.pushedLeft) {
            frame.pushedLeft = true;

            if (frame.node.left != null) {
                var newFrame = new StackFrame();
                newFrame.node = frame.node.left;
                stack.push(newFrame);
                frame = newFrame;
            }
        }

        stack.pop();

        if (frame.node.right != null) {
            var newFrame = new StackFrame();
            newFrame.node = frame.node.right;
            stack.push(newFrame);
        }

        return frame.node.key;
    }

    private class StackFrame {
        boolean pushedLeft = false;
        BinaryTreeNode<T> node;
    }
}
