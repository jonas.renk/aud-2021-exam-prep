package aud.exam.prep.tree;

public class BinaryTreeNode<T> {
    public T key;
    public BinaryTreeNode<T> left;
    public BinaryTreeNode<T> right;
}
