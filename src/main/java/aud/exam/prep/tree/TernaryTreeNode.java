package aud.exam.prep.tree;

public class TernaryTreeNode<T> {
    public T key1;
    public T key2;
    public TernaryTreeNode<T> left;
    public TernaryTreeNode<T> middle;
    public TernaryTreeNode<T> right;
}
