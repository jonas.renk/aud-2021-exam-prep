package aud.exam.prep.tree;

import aud.exam.prep.array.Arrays;

@SuppressWarnings("ManualArrayCopy")
public class Stack<T> {

    private int size = 0;
    private T[] theStack = Arrays.newArray(10);

    public void push(T t) {
        while (size >= theStack.length) {
            T[] newStack = Arrays.newArray(theStack.length * 2);

            for (int i = 0; i < theStack.length; i++) {
                newStack[i] = theStack[i];
            }

            theStack = newStack;
        }

        theStack[size++] = t;
    }

    public T peek() {
        return theStack[size-1];
    }

    public T pop() {
        return theStack[--size];
    }

    public boolean empty() {
        return size == 0;
    }
}
