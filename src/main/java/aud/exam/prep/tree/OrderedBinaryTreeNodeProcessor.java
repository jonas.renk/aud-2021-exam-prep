package aud.exam.prep.tree;

abstract class OrderedBinaryTreeNodeProcessor<V> implements OrderedTreeProcessor<V, BinaryTreeNode<V>> {

    @Override
    public BinaryTreeNode<V> newEmptyTree() {
        return null;
    }

    @Override
    public Iterable<V> iterate(BinaryTreeNode<V> tree) {
        return () ->
            new BinaryTreeIterator<>(tree);
    }
}