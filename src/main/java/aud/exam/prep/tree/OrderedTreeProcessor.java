package aud.exam.prep.tree;

import aud.exam.prep.Pointer;

import java.util.Comparator;
import java.util.function.Predicate;

/**
 * Instances of this interface can work with any "valid" tree of type <code>T</code>.
 * For what is valid, see {@link #check(T, Comparator)}.
 * For this processor trees always have to be "in order".
 * This is a literal translation of the "inspiration sheet" into java.
 *
 * @param <V> Type of elements stored in <code>T</code>
 * @param <T> Type of the tree
 */
public interface OrderedTreeProcessor<V, T> {

    boolean find(T t, V v, Comparator<V> cmp);

    boolean override(Pointer<T> t, V from, V to, Comparator<V> cmp);

    boolean overrideAll(Pointer<T> t, V from, V to, Comparator<V> cmp);

    T insert(T t, V v, Comparator<V> cmp);

    boolean remove(Pointer<T> t, V v, Comparator<V> cmp);

    boolean removeAll(Pointer<T> t, V v, Comparator<V> cmp);

    T removeIf(T t, Predicate<V> pred);

    V max(T t);

    V secondMax(T t);

    int height(T t);

    boolean isBalanced(T t);

    int numberOfNodes(T t);

    int numberOfNodesOnLevel(T t, int level);

    T rightmostNodeInLeftSubtree(T t);

    T leftmostNodeInRightSubtree(T t);

    void invert(T t);

    T clone(T t);

    /**
     * Check whether ot not the given tree is "valid" according to
     * what is considered valid depends on the specific data structure.
     * This is also used by the tests to verify implementations.
     * This has to be tested separate for each data structure.
     *
     * @param t A tree
     * @return true iff the given tree is valid
     */
    boolean check(T t, Comparator<V> cmp);

    /**
     * Create an empty tree.
     *
     * @return A new, empty tree
     */
    T newEmptyTree();

    /**
     * Return an {@link Iterable}, or a lambda returning {@link java.util.Iterator},
     * over the elements of the tree <code>t</code>.
     * This is required by the tests.
     *
     * @param t A tree
     * @return An {@link Iterable} over t
     */
    Iterable<V> iterate(T t);

    /**
     * Print a tree to standard out
     *
     * @param t A tree
     */
    void print(T t);
}