package aud.exam.prep.tree;

import aud.exam.prep.Pointer;

import java.util.Comparator;
import java.util.function.Predicate;

public class RecursiveOrderedBinaryTreeNodeProcessor<V> extends OrderedBinaryTreeNodeProcessor<V> {

    @Override
    public boolean find(BinaryTreeNode<V> tree, V v, Comparator<V> cmp) {
        if (tree == null) {
            return false;
        }

        var c = cmp.compare(v, tree.key);

        if (c == 0) {
            return true;
        }

        if (c < 0) {
            return find(tree.left, v, cmp);
        }

        return find(tree.right, v, cmp);
    }

    @Override
    public boolean override(Pointer<BinaryTreeNode<V>> pointer, V from, V to, Comparator<V> cmp) {
        if (remove(pointer, from, cmp)) {
            pointer.deref = insert(pointer.deref, to, cmp);
            return true;
        }
        return false;
    }

    @Override
    public boolean overrideAll(Pointer<BinaryTreeNode<V>> pointer, V from, V to, Comparator<V> cmp) {
        var count = removeAllWithCount(pointer, from, cmp);
        if (count == 0) {
            return false;
        }

        pointer.deref = insertNTimes(pointer.deref, to, cmp, count);
        return true;
    }

    private BinaryTreeNode<V> insertNTimes(BinaryTreeNode<V> tree, V v, Comparator<V> cmp, int count) {
        if (count == 0) {
            return tree;
        }
        tree = insert(tree, v, cmp);
        return insertNTimes(tree, v, cmp, count-1);
    }

    @Override
    public BinaryTreeNode<V> insert(BinaryTreeNode<V> tree, V v, Comparator<V> cmp) {
        if (tree == null) {
            tree = new BinaryTreeNode<>();
            tree.key = v;
            return tree;
        }

        var c = cmp.compare(v, tree.key);

        if (c < 0 || c == 0 && tree.right != null) {
            tree.left = insert(tree.left, v, cmp);
        } else {
            tree.right = insert(tree.right, v, cmp);
        }

        return tree;
    }

    @Override
    public boolean remove(Pointer<BinaryTreeNode<V>> pointer, V v, Comparator<V> cmp) {
        var tree = pointer.deref;

        if (tree == null) {
            return false;
        }

        var c = cmp.compare(v, tree.key);

        if (c == 0) {
            if (tree.left == null) {
                pointer.deref = tree.right;
            } else if (tree.right == null) {
                pointer.deref = tree.left;
            } else {
                tree.key = getReplacementForRemoval(tree.left, tree, false);
            }

            return true;
        }

        if (c < 0) {
            return removeRec(tree.left, v, cmp, tree, false);
        }

        return removeRec(tree.right, v, cmp, tree, true);
    }

    private boolean removeRec(BinaryTreeNode<V> node, V v, Comparator<V> cmp, BinaryTreeNode<V> prev, boolean right) {
        if (node == null) {
            return false;
        }

        var c = cmp.compare(v, node.key);

        if (c == 0) {
            if (node.left == null) {
                if (right) {
                    prev.right = node.right;
                } else {
                    prev.left = node.right;
                }
            } else if (node.right == null) {
                if (right) {
                    prev.right = node.left;
                } else {
                    prev.left = node.left;
                }
            } else {
                node.key = getReplacementForRemoval(node.left, node, false);
            }

            return true;
        }

        if (c < 0) {
            return removeRec(node.left, v, cmp, node, false);
        }

        return removeRec(node.right, v, cmp, node, true);
    }

    private V getReplacementForRemoval(BinaryTreeNode<V> node, BinaryTreeNode<V> prev, boolean right) {
        if (node.right == null) {
            if (right) {
                prev.right = node.left;
            } else {
                prev.left = node.left;
            }
            return node.key;
        }

        return getReplacementForRemoval(node.right, node, true);
    }

    @Override
    public boolean removeAll(Pointer<BinaryTreeNode<V>> pointer, V v, Comparator<V> cmp) {
        return removeAllWithCount(pointer, v, cmp) > 0;
    }

    private int removeAllWithCount(Pointer<BinaryTreeNode<V>> pointer, V v, Comparator<V> cmp) {
        var tree = pointer.deref;

        if (tree == null) {
            return 0;
        }

        var c = cmp.compare(v, tree.key);

        if (c == 0) {
            if (tree.left == null) {
                pointer.deref = tree.right;
            } else if (tree.right == null) {
                pointer.deref = tree.left;
            } else {
                tree.key = getReplacementForRemoval(tree.left, tree, false);
            }

            return 1 + removeAllWithCount(pointer, v, cmp);
        }

        if (c < 0) {
            return removeAllWithCountRec(tree.left, v, cmp, tree, false);
        }

        return removeAllWithCountRec(tree.right, v, cmp, tree, true);
    }

    private int removeAllWithCountRec(BinaryTreeNode<V> node, V v, Comparator<V> cmp, BinaryTreeNode<V> prev, boolean right) {
        if (node == null) {
            return 0;
        }

        var c = cmp.compare(v, node.key);

        if (c == 0) {
            if (node.left == null) {
                if (right) {
                    prev.right = node.right;
                } else {
                    prev.left = node.right;
                }
            } else if (node.right == null) {
                if (right) {
                    prev.right = node.left;
                } else {
                    prev.left = node.left;
                }
            } else {
                node.key = getReplacementForRemoval(node.left, node, false);
            }

            if (right) {
                node = prev.right;
            } else {
                node = prev.left;
            }

            return 1 + removeAllWithCountRec(node, v, cmp, prev, right);
        }

        if (c < 0) {
            return removeAllWithCountRec(node.left, v, cmp, node, false);
        }

        return removeAllWithCountRec(node.right, v, cmp, node, true);
    }

    @Override
    public BinaryTreeNode<V> removeIf(BinaryTreeNode<V> root, Predicate<V> pred) {
        if (root == null) {
            return null;
        }
        root.left = removeIf(root.left, pred);
        root.right = removeIf(root.right, pred);

        if (pred.test(root.key)) {

            if (root.left == null) {
                return root.right;
            } else if (root.right == null) {
                return root.left;
            } else {
                root.key = removeRightMostNode(root.left, root, false);
            }
        }

        return root;
    }

    V removeRightMostNode(BinaryTreeNode<V> node, BinaryTreeNode<V> prev, boolean right) {
        if (node.right != null) {
            return removeRightMostNode(node.right, node, true);
        }

        if (right) {
            prev.right = node.left;
        } else {
            prev.left = node.left;
        }

        return node.key;
    }

    @Override
    public V max(BinaryTreeNode<V> tree) {
        if (tree == null) {
            throw new IllegalArgumentException("An empty tree has no max");
        }

        if (tree.right == null) {
            return tree.key;
        }

        return max(tree.right);
    }

    @Override
    public V secondMax(BinaryTreeNode<V> tree) {
        if (tree == null) {
            throw new IllegalArgumentException("An empty tree has no second max");
        }

        if (tree.right != null) {
            return secondMaxRec(tree.right, tree.key);
        }

        if (tree.left != null) {
            return max(tree.left);
        }

        throw new IllegalArgumentException("A tree with only one element has no second max");
    }

    private V secondMaxRec(BinaryTreeNode<V> node, V prev) {
        if (node.right != null) {
            return secondMaxRec(node.right, node.key);
        }

        if (node.left != null) {
            return max(node.left);
        }

        return prev;
    }

    @Override
    public int height(BinaryTreeNode<V> tree) {
        if (tree == null) {
            return -1;
        }
        return 0;
    }

    @Override
    public boolean isBalanced(BinaryTreeNode<V> tree) {
        return tree == null;
    }

    @Override
    public int numberOfNodes(BinaryTreeNode<V> tree) {
        return 0;
    }

    @Override
    public int numberOfNodesOnLevel(BinaryTreeNode<V> tree, int level) {
        return 0;
    }

    @Override
    public BinaryTreeNode<V> rightmostNodeInLeftSubtree(BinaryTreeNode<V> tree) {
        return null;
    }

    @Override
    public BinaryTreeNode<V> leftmostNodeInRightSubtree(BinaryTreeNode<V> tree) {
        return null;
    }

    @Override
    public void invert(BinaryTreeNode<V> tree) {
        if (tree == null) {
            return;
        }

        invert(tree.left);
        invert(tree.right);

        var t = tree.left;
        tree.left = tree.right;
        tree.right = t;
    }

    @Override
    public BinaryTreeNode<V> clone(BinaryTreeNode<V> tree) {
        if (tree == null) {
            return null;
        }

        var t = new BinaryTreeNode<V>();
        t.left = clone(tree.left);
        t.right = clone(tree.right);
        t.key = tree.key;

        return t;
    }

    @Override
    public boolean check(BinaryTreeNode<V> tree, Comparator<V> cmp) {
        if (tree == null) {
            return true;
        }

        if (tree.key == null) {
            return false;
        }

        if (tree.left != null) {
            if (!check(tree.left, cmp) || cmp.compare(tree.left.key, tree.key) > 0) {
                return false;
            }
        }

        if (tree.right != null) {
            return check(tree.right, cmp) && cmp.compare(tree.right.key, tree.key) >= 0;
        }

        return true;
    }

    @Override
    public void print(BinaryTreeNode<V> tree) {
        printRec(tree, 0);
    }

    private void printRec(BinaryTreeNode<V> tree, int indent) {
        if (tree == null) {
            System.out.println("- *");
            return;
        }

        System.out.println("-> " + tree.key);

        System.out.print(" |".repeat(indent));
        System.out.print(" L");
        printRec(tree.left, indent + 1);

        System.out.print(" |".repeat(indent));
        System.out.print(" L");
        printRec(tree.right, indent + 1);
    }
}
