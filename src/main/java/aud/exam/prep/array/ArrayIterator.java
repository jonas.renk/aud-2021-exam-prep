package aud.exam.prep.array;

import java.util.Iterator;

public class ArrayIterator<T> implements Iterator<T> {

    private final T[] array;
    private int index;
    private final int end;

    public ArrayIterator(T[] array, int begin, int end) {
        this.array = array;
        this.index = begin;
        this.end = end;
    }

    @Override
    public boolean hasNext() {
        return index < end;
    }

    @Override
    public T next() {
        return array[index++];
    }
}
