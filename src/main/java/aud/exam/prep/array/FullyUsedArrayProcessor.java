package aud.exam.prep.array;

import aud.exam.prep.Pair;
import aud.exam.prep.SequenceProcessor;

abstract class FullyUsedArrayProcessor<T> implements SequenceProcessor<T, FullyUsedArray<T>> {

    @Override
    public boolean overrideAt(FullyUsedArray<T> array, T to, int index) {
        if (index < 0 || index >= array.theArray.length) {
            return false;
        }

        array.theArray[index] = to;
        return true;
    }

    @Override
    public boolean check(FullyUsedArray<T> array) {
        return true;
    }

    @Override
    public Iterable<T> iterate(FullyUsedArray<T> array) {
        return () ->
            new ArrayIterator<>(array.theArray, 0, array.theArray.length);
    }

    protected Pair<FullyUsedArray<T>, FullyUsedArray<T>> createPairForDivideAlternating(FullyUsedArray<T> array) {
        var p = new Pair<FullyUsedArray<T>, FullyUsedArray<T>>();
        p.fst = new FullyUsedArray<>();
        p.snd = new FullyUsedArray<>();

        p.snd.theArray = Arrays.newArray(array.theArray.length / 2);
        p.fst.theArray = Arrays.newArray(array.theArray.length - p.snd.theArray.length);
        return p;
    }

    protected Pair<FullyUsedArray<T>, FullyUsedArray<T>> createPairForDivideByPivot(int less, int greater) {
        var p = new Pair<FullyUsedArray<T>, FullyUsedArray<T>>();
        p.fst = new FullyUsedArray<>();
        p.snd = new FullyUsedArray<>();

        p.fst.theArray = Arrays.newArray(less);
        p.snd.theArray = Arrays.newArray(greater);
        return p;
    }

    protected Pair<FullyUsedArray<T>, FullyUsedArray<T>> createPairForDivideAlternatingByRuns(FullyUsedArray<T> array, int elementsInFst) {
        var p = new Pair<FullyUsedArray<T>, FullyUsedArray<T>>();
        p.fst = new FullyUsedArray<>();
        p.snd = new FullyUsedArray<>();

        p.fst.theArray = Arrays.newArray(elementsInFst);
        p.snd.theArray = Arrays.newArray(array.theArray.length - elementsInFst);
        return p;
    }
}
